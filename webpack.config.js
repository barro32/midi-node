const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: './app/static/bundle.js'
  }
};
